const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require ('mongoose');
const keys = require('../config/keys');


const User = mongoose.model('users');

passport.serializeUser((user, done) => {
    console.log(user.id.toString())
    done(null, user.id);
    console.log("Serializing is ok");
})

passport.deserializeUser((id,done) => {
    User.findById(id).then(user => {
        done(null, user);
        console.log("Deserializing is ok")
    })
})
passport.use(
    new GoogleStrategy({
        clientID: keys.googleClientID,
        clientSecret: keys.googleClientSecret,
        callbackURL: `/auth/google/callback`,
        proxy: true
},
    async (accessToken, refreshToken, profile, done) => {
        console.log(`profile id : ${profile.id}`);
        console.log(profile.id)
        const existingUser = await User.findOne({googleId : profile.id,})
        if (existingUser) {
            done(null, existingUser);
            console.log("Existing User is already inserted in the collection");

        } else {
            const user = await new User({googleId : profile.id}).save()
            console.log("New user need to be inserted in the collection");
            done(null,user)
            ;

        }
    })

);

