import React, { Component } from 'react';

class MainHeader extends Component {
    render() {
        return(

            <nav>
                <div className="nav-wrapper">
                    <a className="left-brand-logo" href="#" className="brand-logo">Book Helper</a>
                    <ul className="right">
                        <li><a href="badges.html">Login with Google</a></li>
                    </ul>
                </div>
            </nav>
        )
    }
};

export default MainHeader