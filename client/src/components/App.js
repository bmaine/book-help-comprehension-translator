import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from '../actions';

import '../App.css';



import MainHeader from './header/MainHeader'
import Landing from './landing/Landing';
import Dashboard from  './dashboard/Dashboard';
import Books from './books/Books'



class App extends Component {
    componentDidMount() {
        this.props.fetchUser();

    }

  render() {
    return (
      <div className="container">
          <BrowserRouter>
              <div>
                  <MainHeader/>
                  <Route exact path="/" component={Landing}/>
                  <Route path="/books" component={Books}/>
                  <Route path="/dashboard" component={Dashboard}/>
              </div>
          </BrowserRouter>
      </div>
    );
  }
}

export default connect(null, actions)(App);
